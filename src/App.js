/* eslint-disable */

import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { hot } from 'react-hot-loader/root'
import { BrowserRouter as Router } from 'react-router-dom'
import Navbar from './components/Navbar'
import ProductList from './components/ProductList'
import Details from './components/Details'
import Cart from './components/Cart'
import Default from './components/Default'
import Footer from './components/Footer'
import Checkout from './components/Checkout'

class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Router>
          <Navbar />
          <main style={{ paddingTop: '64px' }}>
            <Switch>
              <Route exact path="/" component={ProductList} />
              <Route path="/details/:id" component={Details} />
              <Route path="/cart" component={Cart} />
              <Route path="/checkout" component={Checkout} />
              <Route component={Default} />
            </Switch>
          </main>
        </Router>
        <Footer></Footer>
      </React.Fragment>
    )
  }
}
// adding some comments

export default hot(App)
