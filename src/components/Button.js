import styled from 'styled-components'

export const ButtonContainer = styled.button`
  padding: 3px 10px;
  border-radius: 0.3em;
  font-size: 1.4em;
  font-weight: bold;
  background: #43ace3;
  color: #fff;
  float: right;
  margin: 0 0 1.5em 0;
  box-shadow: 0 3px 0 rgba(59, 154, 198, 1);
`
