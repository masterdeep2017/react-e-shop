import React, { Component } from 'react'
import { ProductConsumer } from '../context'
import { Link } from 'react-router-dom'
import './Details.css'
import { Title } from '../components/Title'
import { Swatches } from './Swatches'

class Details extends Component {
  render() {
  
    return (
      <div className="gridDetails">
        <ProductConsumer>
          {value => {
            
            if (
              value.getItem(this.props.match.params.id) === undefined ||
              value.getItem(this.props.match.params.id).length === 0
            ) {
              return <div> ...Loading</div>
            } else {
              // console.log(value, 'value only')
              // console.log(value.getItem(this.props.match.params.id), 'value from details')
              const {
                image,
                tcin,
                title,
                price,
                swatches,
                total_review_count
              } = value.getItem(this.props.match.params.id)
              console.log(swatches,'swatches')
              return (
                <div className="detailsGridTwo">
                  <div className="images">
                    {/* <img src={swatches.color[0].img_url} alt="product"></img> */}
                    <img src={image} alt="product"></img>
                  </div>
                  <div>
                    <div className="detailsGridFour">
                      <div className="detailsGridTitle">{title}</div>
                      <div>{price.formatted_current_price}</div>
                      <div>{price.formatted_current_price}</div>
                    </div>

                    <div className="swatchesGrid">
                      {swatches ? swatches.color.map(swatch => {
                        
                        const changeImage = () => {
                          console.log(swatch.partNumber)
                        }
                            return (
                              <button >
                                <img
                                onClick={() => changeImage()}
                                className="swatches"
                                src={swatch.swatch_url}
                                alt="product"
                              ></img>
                              </button>
                              
                            )
                          })
                        : null}
                    </div>

                    <div>
                      <span className="sizeGrid">Size:</span>
                      <span>See size Table</span>
                      <br></br>
                      <select className="buttonTwo">
                        <option value="Small">Small</option>
                        <option value="Medium">Medium</option>
                        <option value="Large">Large</option>
                        <option value="XL">XL</option>
                      </select>
                    </div>

                    <div className="quanittyGrid">
                      {/* <p>Quantity</p>
                    <button className="button" onClick={() => {value.increment()}}> + </button>
                    <button className="button" onClick={() => {value.decrement()}}> - </button> */}
                      <br></br>
                      <Link to="/cart">
                        <button
                          className="button"
                          onClick={() => {
                            value.addToCart(tcin)
                          }}
                        >
                          Add To Cart
                        </button>
                      </Link>

                      <Link to="/">
                        <button className="button">Back</button>
                      </Link>
                    </div>
                  </div>
                </div>
              )
            }
          }}
        </ProductConsumer>
      </div>
    )
  }
}

export default Details
