import React from 'react'
import './Footer.css'

const Footer = () => {
  return (
    <div className="mainContainer">
      <div className="con">
        <div className="color1">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="42.996"
            height="32.879"
            viewBox="0 0 42.996 32.879"
          >
            <path
              d="M436.271,456.879a1.263,1.263,0,0,1-1.22-.814l-3.062-8.038H418.527A2.527,2.527,0,0,1,416,445.5V426.529A2.528,2.528,0,0,1,418.532,424h17.7a1.264,1.264,0,0,1,1.144.726l3.823,8.126h15.267A2.527,2.527,0,0,1,459,435.381V454.35a2.528,2.528,0,0,1-2.532,2.529Zm-.84-30.35h-16.9s0,6.323,0,18.969h25.828Zm6.961,8.852,5.1,10.842a1.265,1.265,0,0,1-.312,1.49L439.6,454.35h16.864s0-6.323,0-18.969Zm-5.624,18.085,6.216-5.439H434.7Z"
              transform="translate(-416 -424)"
            />
          </svg>
          <div>
            <p>
              this is our wonderful store. you would be very happy to make your
              purchases online in just no time. we ill help you get just what
              you need immediately
            </p>
          </div>
        </div>

        <div className="color2">
          <ul className="footer-nav">
            <li>
              <a href="/">Shopping Online</a>
            </li>
            <br></br>
            <li>
              <a href="/">Oeder Status</a>
            </li>
            <li>
              <a href="/">Shipping Delivery</a>
            </li>
            <li>
              <a href="/">Returns</a>
            </li>
            <li>
              <a href="/">Payment options</a>
            </li>
            <li>
              <a href="/">Contact Us</a>
            </li>
          </ul>
        </div>
        <div className="color3">
          <ul className="footer-nav">
            <li>
              <a href="/">Information</a>
            </li>
            <br></br>
            <li>
              <a href="/">Gift Cards</a>
            </li>
            <li>
              <a href="/">Find a Store</a>
            </li>
            <li>
              <a href="/">News Letter</a>
            </li>
            <li>
              <a href="/">Become a member</a>
            </li>
            <li>
              <a href="/">Site feedback</a>
            </li>
          </ul>
        </div>
        <div className="color4">
          <ul className="footer-nav">
            <li>
              <a href="/">Contact</a>
            </li>
            <br></br>
            <li>
              <a href="/">store@store.com</a>
            </li>
            <li>
              <a href="/">Hotline: +1 131 31231 313</a>
            </li>
          </ul>
        </div>
        {/* <div className="color5">5</div>
            <div className="color6">6</div> */}
      </div>
    </div>

    //   <div>

    //   <footer className="footer">
    //     <div className="footer-container">

    //         <ul className="footer-nav">
    //         <li><a href="#">Shopping Online</a></li>
    //             <li><a href="#">Home</a></li>
    //             <li><a href="#">About Us</a></li>
    //             <li><a href="#">Our Work</a></li>
    //             <li><a href="#">Blog</a></li>
    //         </ul>
    //         <ul className="footer-secondary-nav">
    //             <li><a href="#">Latest News</a></li>
    //             <li><a href="#">Our Partners</a></li>

    //         </ul>
    //         <div className="contact-block">
    //             <h3>We're happy to hear from you!</h3>
    //             <a href="tel:999-999-4321"><i className="fas fa-phone"></i> 999-999-4321</a>
    //             <a href="mailto:info@mycompany.xyz">info@mycompany.xyz</a>
    //         </div>
    //         <div className="newsletter">
    //             <div className="newsletter-text">
    //                 <div className="icon">
    //                     <i className="fas fa-paper-plane"></i>
    //                 </div>
    //                 <p>
    //                     Subscribe to our Newsletter
    //                 </p>
    //             </div>

    //         </div>
    //         <div className="social-networks">
    //             <h3>Share our content!</h3>
    //             <div className="facebook">
    //                 <i className="fab fa-facebook"></i>
    //             </div>
    //             <div className="twitter">
    //                 <i className="fab fa-twitter"></i>
    //             </div>
    //             <div className="instagram">
    //                 <i className="fab fa-instagram"></i>
    //             </div>
    //         </div>
    //     </div>
    // </footer>

    // </div>
  )
}

export default Footer
