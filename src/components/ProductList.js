/* eslint-disable */

import React, { Component } from 'react'
import Product from './Product'
import Title from './Title'
import { ProductConsumer } from '../context'

class ProductList extends Component {
  render() {
    return (
      <React.Fragment>
        <Title name="our" title="product" />
        <div>
          <div className="container">
            <ProductConsumer>
              {value => {
                //  console.log(value, 'this is value')
                return value.items.map(item => {
                  return <Product key={item.tcin} item={item} />
                })
              }}
            </ProductConsumer>
          </div>
        </div>
      </React.Fragment>

      // <Product />
    )
  }
}

export default ProductList
