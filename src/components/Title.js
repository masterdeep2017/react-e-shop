/* eslint-disable */

import React from 'react'
import './Title.css'

export default function Title({ name, title }) {
  return (
    <div className="titleOne">
      <div>
        <p>
          {name}
          <strong> {title}</strong>
        </p>
      </div>
      <div>
        <span>Show Products</span>
        <select className="selectCss"></select>
      </div>
      <div>
        <span>Sort</span>
        <select className="selectCss"></select>
      </div>
    </div>
  )
}
